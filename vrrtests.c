// clang  -Wall -pedantic -O2 -lEGL -lGLESv2 -lgbm -ldrm -lm

#include <EGL/egl.h>
#include <EGL/eglext.h>
#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>
#include <assert.h>
#include <drm_fourcc.h>
#include <errno.h>
#include <fcntl.h>
#include <gbm.h>
#include <math.h>
#include <poll.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <xf86drm.h>
#include <xf86drmMode.h>

// Returns the KMS id of the first plane compatible with the first crtc.
int FindFirstPlaneOrDie(int fd) {
  drmModePlaneRes* plane_res = drmModeGetPlaneResources(fd);
  int ps = -1;
  for (int i = 0; ps == -1 && i < plane_res->count_planes; i++) {
    drmModePlane* plane = drmModeGetPlane(fd, plane_res->planes[i]);
    assert(plane);
    if (plane->possible_crtcs & 1) {
      ps = plane_res->planes[i];
    }
    drmModeFreePlane(plane);
  }

  drmModeFreePlaneResources(plane_res);
  assert(ps != -1);
  return ps;
}

// Returns the value of the KMS property |property_name|.
uint32_t DrmGetPropertyValueOrDie(int fd,
                                  int object_id,
                                  int object_type,
                                  const char* property_name) {
  drmModeObjectPropertiesPtr props =
      drmModeObjectGetProperties(fd, object_id, object_type);
  assert(props);

  uint32_t v = 0;
  int found = 0;
  for (int i = 0; !found && i < props->count_props; ++i) {
    drmModePropertyPtr p = drmModeGetProperty(fd, props->props[i]);
    if (!strcmp(p->name, property_name)) {
      v = props->prop_values[i];
      found = 1;
    }
    drmModeFreeProperty(p);
  }
  drmModeFreeObjectProperties(props);
  assert(found);
  return v;
}

// Returns the id of the first connector.
// |is_edp| determines if the connector is an edp or not (internal or external
// panel). If |is_vrr| is true only connectors that support vrr will be
// returned. |width| and |height| will be filled with the first resolution found
// on the connector.
int FindConnectorOrDie(int fd,
                       int is_edp,
                       int is_vrr,
                       drmModeRes* res,
                       int* width,
                       int* height) {
  int connector_id = -1;
  for (int i = 0; connector_id == -1 && i < res->count_connectors; i++) {
    drmModeConnector* conn = drmModeGetConnector(fd, res->connectors[i]);
    if (conn->connection == DRM_MODE_CONNECTED && conn->count_modes &&
        ((conn->connector_type == DRM_MODE_CONNECTOR_eDP) == is_edp) &&
        (!is_vrr ||
         DrmGetPropertyValueOrDie(fd, conn->connector_id,
                                  DRM_MODE_OBJECT_CONNECTOR, "vrr_capable"))) {
      *width = conn->modes->hdisplay;
      *height = conn->modes->vdisplay;
      connector_id = conn->connector_id;
      printf("Found a connector: %d, resolution %dx%d, eDP: %d, vrr: %d\n",
             connector_id, *width, *height, is_edp, is_vrr);
    }
    drmModeFreeConnector(conn);
  }
  assert(connector_id != -1);
  return connector_id;
}

// Returns the id of the property |property_name| or -1 if it can't be found
int DrmFindPropertyId(int fd,
                      int object_id,
                      int object_type,
                      const char* property_name) {
  drmModeObjectPropertiesPtr props =
      drmModeObjectGetProperties(fd, object_id, object_type);
  assert(props);

  int prop_id = -1;
  for (int i = 0; prop_id == -1 && i < props->count_props; ++i) {
    drmModePropertyPtr prop = drmModeGetProperty(fd, props->props[i]);
    if (!strcmp(prop->name, property_name)) {
      prop_id = prop->prop_id;
    }
    drmModeFreeProperty(prop);
  }
  drmModeFreeObjectProperties(props);
  return prop_id;
}

int DrmFindPropertyIdOrDie(int fd,
                           int object_id,
                           int object_type,
                           const char* property_name) {
  int prop_id = DrmFindPropertyId(fd, object_id, object_type, property_name);
  if (prop_id == -1) {
    fprintf(stderr, "Couldn't find %s\n", property_name);
    abort();
  }
  return prop_id;
}

// Returns the first mode that matches the resolution specified.
// If |*refresh_rate| is 0.0f, the first mode is returned, otherwise the mode
// returned will be close to the specified rate.
int DrmFindFirstModeBlobId(int fd,
                           int connector_id,
                           int width,
                           int height,
                           float* refresh_rate) {
  drmModeConnector* connector = drmModeGetConnector(fd, connector_id);
  uint32_t blob_id = 0;
  for (int i = 0; i < connector->count_modes; ++i) {
    if (connector->modes[i].hdisplay == width &&
        connector->modes[i].vdisplay == height) {
      float clock = connector->modes[i].clock;
      float htotal = connector->modes[i].htotal;
      float vtotal = connector->modes[i].vtotal;
      float rr = (clock * 1000.0f) / (htotal * vtotal);
      if (*refresh_rate == 0.0f || fabs(*refresh_rate - rr) < 10.f) {
        *refresh_rate = rr;
        drmModeCreatePropertyBlob(fd, &connector->modes[i],
                                  sizeof(drmModeModeInfo), &blob_id);
        break;
      }
    }
  }
  drmModeFreeConnector(connector);
  assert(blob_id);
  return blob_id;
}

void VBlankHandler(int fd,
                   unsigned int sequence,
                   unsigned int tv_sec,
                   unsigned int tv_usec,
                   void* user_data) {
  fprintf(stderr, "Ignored vblank handler");
}

void PageFlipHandler(int fd,
                     unsigned int sequence,
                     unsigned int tv_sec,
                     unsigned int tv_usec,
                     unsigned int crtc_id,
                     void* user_data) {
  struct timeval* time = (struct timeval*)(user_data);
  time->tv_sec = tv_sec;
  time->tv_usec = tv_usec;
}

// Opens the first drm device that is not "vgem"
int OpenDrmDevice() {
  int device_fd = -1;
  int i = 0;
  while (device_fd < 0) {
    char dri_device_node[128];
    sprintf(dri_device_node, "/dev/dri/card%u", i++);
    printf("Opening %s\n", dri_device_node);
    device_fd = open(dri_device_node, O_RDWR);
    if (device_fd < 0) {
      fprintf(stderr, "Couldn't find a drm device.\n");
      return device_fd;
    }
    drmVersionPtr drm_version = drmGetVersion(device_fd);
    if (!drm_version) {
      fprintf(stderr, "Can't get version for device %s\n", dri_device_node);
      close(device_fd);
      device_fd = -1;
    } else if (strstr(drm_version->name, "vgem")) {
      fprintf(stderr, "Skipping device %s\n", drm_version->name);
      close(device_fd);
      device_fd = -1;
    } else {
      printf("Found device %s\n", drm_version->name);
    }
    drmFreeVersion(drm_version);
  }

  return device_fd;
}

// This struct contains the state to manage the display, and the bos allocated
// for the front and back bufers.
struct DisplayAndBos {
  int device_fd;
  struct gbm_device* gbm;
  struct gbm_bo* bos[2];
  uint32_t fbs[2];
  int plane_id;
  int crtc_id;
  int fb_property_id;
  int in_fence_property_id;
  int out_fence_property_id;
  drmModeAtomicReqPtr atomic_req;
  int cursor;
};

// Init the display via KMS and allocates the bos.
// This does not modeset the display.
struct DisplayAndBos InitDisplayAndBos(int is_edp,
                                       int is_vrr,
                                       float refresh_rate) {
  struct DisplayAndBos d = {0};
  d.device_fd = OpenDrmDevice();
  assert(d.device_fd >= 0);

  int ret = drmSetClientCap(d.device_fd, DRM_CLIENT_CAP_UNIVERSAL_PLANES, 1);
  assert(!ret);
  ret = drmSetClientCap(d.device_fd, DRM_CLIENT_CAP_ATOMIC, 1);
  assert(!ret);

  drmModeRes* res = drmModeGetResources(d.device_fd);

  int width = 0, height = 0;
  int connector_id =
      FindConnectorOrDie(d.device_fd, is_edp, is_vrr, res, &width, &height);
  // Assume the first crtc is good.
  assert(res->count_crtcs);
  d.crtc_id = *res->crtcs;
  drmModeFreeResources(res);

  d.gbm = gbm_create_device(d.device_fd);
  assert(d.gbm);

  for (int i = 0; i < 2; ++i) {
    int pixel_format = DRM_FORMAT_XRGB8888;
    d.bos[i] = gbm_bo_create(d.gbm, width, height, pixel_format,
                             GBM_BO_USE_SCANOUT | GBM_BO_USE_RENDERING);
    assert(d.bos[i]);
    assert(gbm_bo_get_plane_count(d.bos[i]) == 1);

    uint32_t bo_handles[4] = {gbm_bo_get_handle(d.bos[i]).u32};
    uint32_t bo_pitches[4] = {gbm_bo_get_stride(d.bos[i])};
    uint32_t bo_offsets[4] = {0};

    ret = drmModeAddFB2WithModifiers(d.device_fd, width, height, pixel_format,
                                     bo_handles, bo_pitches, bo_offsets, 0,
                                     &d.fbs[i], 0);
    assert(!ret);
  }

  d.plane_id = FindFirstPlaneOrDie(d.device_fd);
  int property_id = 0;
  d.atomic_req = drmModeAtomicAlloc();
  property_id = DrmFindPropertyIdOrDie(d.device_fd, connector_id,
                                       DRM_MODE_OBJECT_CONNECTOR, "CRTC_ID");
  drmModeAtomicAddProperty(d.atomic_req, connector_id, property_id, d.crtc_id);

  // Set crtc property
  property_id = DrmFindPropertyIdOrDie(d.device_fd, d.crtc_id,
                                       DRM_MODE_OBJECT_CRTC, "MODE_ID");
  drmModeAtomicAddProperty(
      d.atomic_req, d.crtc_id, property_id,
      DrmFindFirstModeBlobId(d.device_fd, connector_id, width, height,
                             &refresh_rate));
  printf("Refresh rate: %f\n", refresh_rate);
  property_id = DrmFindPropertyId(d.device_fd, d.crtc_id, DRM_MODE_OBJECT_CRTC,
                                  "VRR_ENABLED");
  if (property_id != -1) {
    drmModeAtomicAddProperty(d.atomic_req, d.crtc_id, property_id, is_vrr);
  }
  property_id = DrmFindPropertyIdOrDie(d.device_fd, d.crtc_id,
                                       DRM_MODE_OBJECT_CRTC, "ACTIVE");
  drmModeAtomicAddProperty(d.atomic_req, d.crtc_id, property_id, 1);

  d.fb_property_id = DrmFindPropertyIdOrDie(d.device_fd, d.plane_id,
                                            DRM_MODE_OBJECT_PLANE, "FB_ID");
  d.in_fence_property_id = DrmFindPropertyIdOrDie(
      d.device_fd, d.plane_id, DRM_MODE_OBJECT_PLANE, "IN_FENCE_FD");
  d.out_fence_property_id = DrmFindPropertyIdOrDie(
      d.device_fd, d.crtc_id, DRM_MODE_OBJECT_CRTC, "OUT_FENCE_PTR");

  struct {
    char* name;
    int value;
  } plane_props[] = {
      {"CRTC_ID", d.crtc_id}, {"CRTC_X", 0},          {"CRTC_Y", 0},
      {"CRTC_W", width},      {"CRTC_H", height},     {"SRC_X", 0},
      {"SRC_Y", 0},           {"SRC_W", width << 16}, {"SRC_H", height << 16},
  };
  for (int i = 0; i < sizeof(plane_props) / sizeof(plane_props[0]); i++) {
    property_id = DrmFindPropertyIdOrDie(
        d.device_fd, d.plane_id, DRM_MODE_OBJECT_PLANE, plane_props[i].name);
    drmModeAtomicAddProperty(d.atomic_req, d.plane_id, property_id,
                             plane_props[i].value);
  }
  d.cursor = drmModeAtomicGetCursor(d.atomic_req);

  return d;
}

// Modesets the display using the bos in |d|
void ModesetOrDie(struct DisplayAndBos d) {
  drmModeAtomicAddProperty(d.atomic_req, d.plane_id, d.fb_property_id,
                           d.fbs[0]);
  int ret = drmModeAtomicCommit(d.device_fd, d.atomic_req,
                                DRM_MODE_ATOMIC_ALLOW_MODESET, 0);
  assert(!ret);
}

void DestroyDisplayAndBos(struct DisplayAndBos d) {
  for (int i = 0; i < 2; i++) {
    drmModeRmFB(d.device_fd, d.fbs[i]);
    gbm_bo_destroy(d.bos[i]);
  }
  gbm_device_destroy(d.gbm);
  drmModeAtomicFree(d.atomic_req);
  close(d.device_fd);
}

// Struct used to keep GL and EGL state, including the two GL framebuffers used
// for front and back buffers.
struct Gpu {
  EGLDisplay display;
  EGLContext context;
  PFNEGLCREATESYNCKHRPROC eglCreateSyncKHR;
  PFNEGLDESTROYSYNCKHRPROC eglDestroySyncKHR;
  PFNEGLWAITSYNCKHRPROC eglWaitSyncKHR;
  PFNEGLDUPNATIVEFENCEFDANDROIDPROC eglDupNativeFenceFDANDROID;
  GLuint fbs[2];
};

// Initialize EGL/GL. Create GL framebuffers from |bos|.
struct Gpu InitGpu(struct gbm_bo* bos[]) {
  assert(bos);
  struct Gpu gpu;
  const char* eglExtensions = eglQueryString(EGL_NO_DISPLAY, EGL_EXTENSIONS);
  assert(eglExtensions);
  assert(strstr(eglExtensions, "EGL_EXT_platform_base"));
  assert(strstr(eglExtensions, "EGL_MESA_platform_surfaceless"));
  PFNEGLGETPLATFORMDISPLAYEXTPROC eglGetPlatformDisplayEXT =
      (PFNEGLGETPLATFORMDISPLAYEXTPROC)eglGetProcAddress(
          "eglGetPlatformDisplayEXT");
  assert(eglGetPlatformDisplayEXT);
  PFNEGLCREATEIMAGEKHRPROC eglCreateImageKHR =
      (PFNEGLCREATEIMAGEKHRPROC)eglGetProcAddress("eglCreateImageKHR");
  assert(eglCreateImageKHR);
  PFNEGLDESTROYIMAGEKHRPROC eglDestroyImageKHR =
      (PFNEGLDESTROYIMAGEKHRPROC)eglGetProcAddress("eglDestroyImageKHR");
  assert(eglDestroyImageKHR);
  PFNGLEGLIMAGETARGETTEXTURE2DOESPROC eglImageTargetTexture2DOES =
      (PFNGLEGLIMAGETARGETTEXTURE2DOESPROC)eglGetProcAddress(
          "glEGLImageTargetTexture2DOES");
  assert(eglImageTargetTexture2DOES);

  // Setup EGL
  gpu.display = eglGetPlatformDisplayEXT(EGL_PLATFORM_SURFACELESS_MESA,
                                         (void*)EGL_DEFAULT_DISPLAY, 0);
  assert(gpu.display != EGL_NO_DISPLAY);
  EGLint major, minor;
  EGLBoolean ret = eglInitialize(gpu.display, &major, &minor);
  assert(ret);

  eglExtensions = eglQueryString(gpu.display, EGL_EXTENSIONS);
  assert(strstr(eglExtensions, "EGL_KHR_fence_sync"));
  gpu.eglCreateSyncKHR =
      (PFNEGLCREATESYNCKHRPROC)eglGetProcAddress("eglCreateSyncKHR");
  assert(gpu.eglCreateSyncKHR);
  gpu.eglDestroySyncKHR =
      (PFNEGLDESTROYIMAGEKHRPROC)eglGetProcAddress("eglDestroySyncKHR");
  assert(gpu.eglDestroySyncKHR);
  gpu.eglWaitSyncKHR =
      (PFNEGLWAITSYNCKHRPROC)eglGetProcAddress("eglWaitSyncKHR");
  assert(gpu.eglWaitSyncKHR);
  gpu.eglWaitSyncKHR =
      (PFNEGLWAITSYNCKHRPROC)eglGetProcAddress("eglWaitSyncKHR");
  assert(gpu.eglWaitSyncKHR);
  assert(strstr(eglExtensions, "EGL_ANDROID_native_fence_sync"));
  gpu.eglDupNativeFenceFDANDROID =
      (PFNEGLDUPNATIVEFENCEFDANDROIDPROC)eglGetProcAddress(
          "eglDupNativeFenceFDANDROID");
  assert(gpu.eglDupNativeFenceFDANDROID);

  ret = eglBindAPI(EGL_OPENGL_ES_API);
  assert(ret);
  EGLint attr[] = {EGL_BUFFER_SIZE,
                   32,
                   EGL_RED_SIZE,
                   8,
                   EGL_GREEN_SIZE,
                   8,
                   EGL_BLUE_SIZE,
                   8,
                   EGL_ALPHA_SIZE,
                   8,
                   EGL_RENDERABLE_TYPE,
                   EGL_OPENGL_ES2_BIT,
                   EGL_SURFACE_TYPE,
                   EGL_DONT_CARE,
                   EGL_NONE};
  EGLConfig config;
  EGLint num_config;
  EGLint r = eglChooseConfig(gpu.display, attr, &config, 1, &num_config);
  assert(r != EGL_FALSE);
  assert(num_config == 1);

  // GLES3.0
  EGLint context_attribs[] = {EGL_CONTEXT_CLIENT_VERSION, 3, EGL_NONE,
                              EGL_NONE};
  gpu.context = eglCreateContext(gpu.display, config, 0, context_attribs);
  assert(gpu.context != EGL_NO_CONTEXT);
  ret =
      eglMakeCurrent(gpu.display, EGL_NO_SURFACE, EGL_NO_SURFACE, gpu.context);
  assert(ret);
  printf("GL Version%s\n", glGetString(GL_VERSION));

  glGenFramebuffers(2, gpu.fbs);
  for (int i = 0; i < 2; i++) {
    struct gbm_bo* bo = bos[i];
    int fd = gbm_bo_get_fd(bo);
    EGLint attrs[] = {EGL_WIDTH,
                      gbm_bo_get_width(bo),
                      EGL_HEIGHT,
                      gbm_bo_get_height(bo),
                      EGL_LINUX_DRM_FOURCC_EXT,
                      gbm_bo_get_format(bo),
                      EGL_DMA_BUF_PLANE0_FD_EXT,
                      fd,
                      EGL_DMA_BUF_PLANE0_OFFSET_EXT,
                      0,
                      EGL_DMA_BUF_PLANE0_PITCH_EXT,
                      gbm_bo_get_stride(bo),
                      EGL_DMA_BUF_PLANE0_MODIFIER_LO_EXT,
                      gbm_bo_get_modifier(bo) & 0xffffffff,
                      EGL_DMA_BUF_PLANE0_MODIFIER_HI_EXT,
                      gbm_bo_get_modifier(bo) >> 32,
                      EGL_NONE};

    EGLImage image = eglCreateImageKHR(gpu.display, EGL_NO_CONTEXT,
                                       EGL_LINUX_DMA_BUF_EXT, 0, attrs);
    assert(image != EGL_NO_IMAGE_KHR);
    close(fd);
    GLuint texture;
    glGenTextures(1, &texture);

    glBindTexture(GL_TEXTURE_2D, texture);
    eglImageTargetTexture2DOES(GL_TEXTURE_2D, image);
    eglDestroyImageKHR(gpu.display, image);

    glBindFramebuffer(GL_FRAMEBUFFER, gpu.fbs[i]);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D,
                           texture, 0);
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);
    assert(GL_FRAMEBUFFER_COMPLETE == glCheckFramebufferStatus(GL_FRAMEBUFFER));
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glDeleteTextures(1, &texture);
  }
  glViewport(0, 0, gbm_bo_get_width(*bos), gbm_bo_get_height(*bos));
  glFlush();

  return gpu;
}

void DestroyGpuObjects(struct Gpu gpu) {
  glDeleteFramebuffers(2, gpu.fbs);
  eglMakeCurrent(gpu.display, EGL_NO_SURFACE, EGL_NO_SURFACE, EGL_NO_CONTEXT);
  eglDestroyContext(gpu.display, gpu.context);
}

// Struct used to keep the state of the scrolling html skeleton and the GL
// objects needed to draw it
struct ScrollingState {
  int fractal_iteration;
  GLuint program;
  GLint color_uniform;
  GLint transform_uniform;
  GLint type_uniform;
  GLint iter_uniform;
  GLint seed_uniform;
  int words_per_paragraph[64][64];
  struct timeval last_time;
};

float kCharSize = 0.02f;
float kImageSize = 0.2f;
float kPadding = 0.03f;
float kParagraphWidth = 0.8f;
float ComputeParagraphHeight(int words[], int size) {
  float length = 0.0f;
  float height = kImageSize + kPadding * 2.0f;
  for (int w = 0; w < size && words[w]; w++) {
    float word_length = words[w] * kCharSize;
    length += word_length;
    if (length >= kParagraphWidth - kPadding) {
      length = word_length;
      height += kCharSize + kPadding;
    }
  }
  return height + kCharSize + kPadding;
}

float ScrollingFunction(struct timeval time) {
  double millis = time.tv_sec * 1000. + time.tv_usec / 1000.;
  return fabs(fmod(millis, 10000.f) - 5000.) / 1000.;
}

float* QuadMatrix(float top, float left, float width, float height) {
  static float m[9] = {0.0f};
  m[0] = width * .5f;
  m[4] = height * .5f;
  m[6] = width * 0.5f + left;
  m[7] = height * 0.5f + top;
  m[8] = 1.0f;
  return m;
}

// Draws an html skeleton starting as defined in |scroll|
int DrawFrame(struct Gpu* gpu,
              int frame,
              int out_fence_fd,
              struct timeval last_present_time,
              struct ScrollingState* scroll) {
  if (out_fence_fd != -1) {
    // Wait service side
    EGLint attribs[] = {EGL_SYNC_NATIVE_FENCE_FD_ANDROID, out_fence_fd,
                        EGL_NONE};
    EGLSyncKHR out_sync = gpu->eglCreateSyncKHR(
        gpu->display, EGL_SYNC_NATIVE_FENCE_ANDROID, attribs);
    gpu->eglWaitSyncKHR(gpu->display, out_sync, 0);
    gpu->eglDestroySyncKHR(gpu->display, out_sync);
  }

  glBindFramebuffer(GL_FRAMEBUFFER, gpu->fbs[frame % 2]);
  glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
  glClear(GL_COLOR_BUFFER_BIT);

  float y_start = -1.0f + kPadding - ScrollingFunction(last_present_time);
  float y = y_start;
  glUniform4f(scroll->color_uniform, 0.8f, 0.8f, 0.8f, 1.0f);
  glUniform1i(scroll->type_uniform, 0);
  // Draw paragraph box
  int paragraphs = sizeof(scroll->words_per_paragraph) /
                   sizeof(scroll->words_per_paragraph[0]);
  for (int p = 0; p < paragraphs; p++) {
    int words = sizeof(scroll->words_per_paragraph[0]) /
                sizeof(scroll->words_per_paragraph[0][0]);
    float height =
        ComputeParagraphHeight(scroll->words_per_paragraph[p], words);
    glUniformMatrix3fv(scroll->transform_uniform, 1, 0,
                       QuadMatrix(y, -1.0f + ((2.0 - kParagraphWidth) * 0.5f),
                                  kParagraphWidth, height));
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    y += height + kPadding;
  }
  // Draw each paragraph
  y = y_start;
  for (int p = 0; p < paragraphs; p++) {
    // Draw the image
    float paragraph_left = -1.0f + ((2.0 - kParagraphWidth) * 0.5f);

    glUniform4f(scroll->color_uniform, 0.4f, 0.4f, 0.4f, 1.0f);
    glUniform1i(scroll->type_uniform, 2);
    glUniform1i(scroll->iter_uniform, scroll->fractal_iteration);
    glUniform2f(scroll->seed_uniform, -0.64f, 0.96f);
    glUniformMatrix3fv(scroll->transform_uniform, 1, 0,
                       QuadMatrix(y + kPadding, paragraph_left + kPadding,
                                  kImageSize, kImageSize));
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

    // Draw text skeleton
    glUniform1i(scroll->type_uniform, 1);
    int words = sizeof(scroll->words_per_paragraph[0]) /
                sizeof(scroll->words_per_paragraph[0][0]);
    float length = 0.0f;
    y += kImageSize + kPadding * 2.0f;
    for (int w = 0; w < words && scroll->words_per_paragraph[p][w]; w++) {
      float word_length = scroll->words_per_paragraph[p][w] * kCharSize;
      length += word_length;
      if (length >= kParagraphWidth - kPadding) {
        length = word_length;
        y += kCharSize + kPadding;
      }
      glUniformMatrix3fv(
          scroll->transform_uniform, 1, 0,
          QuadMatrix(y, paragraph_left + kPadding + length - word_length,
                     word_length - kCharSize, kCharSize));
      glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    }
    y += kCharSize + kPadding * 2.0f;
  }

  EGLSyncKHR sync =
      gpu->eglCreateSyncKHR(gpu->display, EGL_SYNC_NATIVE_FENCE_ANDROID, 0);
  int in_fence_fd = gpu->eglDupNativeFenceFDANDROID(gpu->display, sync);
  gpu->eglDestroySyncKHR(gpu->display, sync);
  return in_fence_fd;
}

struct CmdLineParameters {
  int external;
  int vrr;
  float refresh_rate;
  int fractal_iteration;
  int help;
};

void PrintHelp(char* name) {
  fprintf(stderr, "usage: %s [-evri?]\n", name);

  fprintf(stderr, "\n options:\n\n");
  fprintf(stderr, "\t-e\tuses an external connector\n");
  fprintf(stderr, "\t-v\tuses VRR\n");
  fprintf(stderr, "\t-r\t<refresh> uses a specific refresh rate\n");
  fprintf(stderr, "\t-i\tsets the fractal iteration number\n");
}

struct CmdLineParameters ParseCmdLine(int argc, char** argv) {
  struct CmdLineParameters p = {0};
  int c;
  p.fractal_iteration = 100;
  while ((c = getopt(argc, argv, "?evr:i:")) != -1) {
    switch (c) {
      case 'e':
        p.external = 1;
        break;
      case 'v':
        p.vrr = 1;
        break;
      case 'r':
        p.refresh_rate = atof(optarg);
        break;
      case 'i':
        p.fractal_iteration = atoi(optarg);
        break;
      case '?':
        p.help = 1;
        break;
      default:
        abort();
    }
  }
  return p;
}

GLuint LoadShaderOrDie(GLenum type, const GLchar* const src) {
  GLuint shader = 0;
  shader = glCreateShader(type);
  assert(shader != 0);
  glShaderSource(shader, 1, &src, 0);
  glCompileShader(shader);
  GLint compiled = 0;
  glGetShaderiv(shader, GL_COMPILE_STATUS, &compiled);
  if (compiled == 0) {
    GLint len = 0;
    glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &len);
    if (len > 0) {
      assert(len < 1024);
      char err[1024];
      glGetShaderInfoLog(shader, len, 0, err);
      fprintf(stderr, "Error compiling shader: %s\n", err);
      abort();
    }
  }

  return shader;
}

#define SHADER(Src)   \
  "#version 300 es\n" \
  "precision mediump float;\n" #Src
const char kQuadVertexShader[] =
    SHADER(uniform mat3 transform; out vec2 tex_pos; void main() {
      vec2 pos[4];
      pos[0] = vec2(-1.0, -1.0);
      pos[1] = vec2(1.0, -1.0);
      pos[2] = vec2(-1.0, 1.0);
      pos[3] = vec2(1.0, 1.0);
      gl_Position.xy = pos[gl_VertexID];
      gl_Position.zw = vec2(1.0, 1.0);
      tex_pos = (gl_Position.xy + vec2(1.0, 1.0)) * 0.5;
      gl_Position.xyz = transform * gl_Position.xyz;
    });

const char kFragmentShader[] = SHADER(
    in vec2 tex_pos; uniform vec2 seed; uniform vec4 color; uniform int type;
    uniform int iter;
    out vec4 out_color;
    vec4 fractalColor(float i) {
      return vec4(i, i / 1.2, i / 6.0, 1.0);
    } vec4 JuliaFractal(vec2 pos) {
      vec2 z;
      z.x = 3.0 * (pos.x - 0.5);
      z.y = 2.0 * (pos.y - 0.5);
      int i;
      int i2;
      for (i = 0; i < iter; i++) {
        float x = (z.x * z.x - z.y * z.y) + seed.x;
        float y = (z.y * z.x + z.x * z.y) + seed.y;

        if ((x * x + y * y) > 4.0)
          break;
        z.x = x;
        z.y = y;
      }
      return fractalColor(i == iter ? 0.0 : float(i) / 100.0);
    } vec4 JuliaFractalMultiSampled(vec2 pos) {
      vec4 o = JuliaFractal(pos);
      o += JuliaFractal(vec2(tex_pos.x + dFdx(tex_pos.x), tex_pos.y));
      o += JuliaFractal(vec2(tex_pos.x, tex_pos.y + dFdy(tex_pos.y)));
      o += JuliaFractal(
          vec2(tex_pos.x + dFdx(tex_pos.x), tex_pos.y + dFdy(tex_pos.y)));
      return o / 4.0;
    } void main() {
      if (type == 0) {
        out_color = color;
      } else if (type == 1) {
        out_color.rgb =
            color.rgb * (1.0 + cos(tex_pos.x)) * (1.0 * sin(tex_pos.y));
      } else if (type == 2) {
        out_color = JuliaFractalMultiSampled(tex_pos);
      }
    });

struct ScrollingState InitScrollingState() {
  struct ScrollingState s = {0};
  GLuint quad_vertex = LoadShaderOrDie(GL_VERTEX_SHADER, kQuadVertexShader);
  GLuint frag = LoadShaderOrDie(GL_FRAGMENT_SHADER, kFragmentShader);

  s.program = glCreateProgram();
  glAttachShader(s.program, quad_vertex);
  glAttachShader(s.program, frag);
  glDeleteShader(quad_vertex);
  glDeleteShader(frag);
  glLinkProgram(s.program);

  GLint linked = -1;
  glGetProgramiv(s.program, GL_LINK_STATUS, &linked);
  assert(linked);
  glUseProgram(s.program);
  s.color_uniform = glGetUniformLocation(s.program, "color");
  s.transform_uniform = glGetUniformLocation(s.program, "transform");
  s.type_uniform = glGetUniformLocation(s.program, "type");
  s.iter_uniform = glGetUniformLocation(s.program, "iter");
  s.seed_uniform = glGetUniformLocation(s.program, "seed");

  for (int p = 0;
       p < sizeof(s.words_per_paragraph) / sizeof(s.words_per_paragraph[0]);
       p++) {
    int words = rand() % (sizeof(s.words_per_paragraph[0]) /
                          sizeof(s.words_per_paragraph[0][0]));
    for (int w = 0; w < words; w++) {
      s.words_per_paragraph[p][w] = rand() % 12 + 2;
    }
  }
  return s;
}

void DestroyScrollingState(struct ScrollingState scroll) {
  glDeleteProgram(scroll.program);
}

int main(int argc, char** argv) {
  struct CmdLineParameters p = ParseCmdLine(argc, argv);
  if (p.help) {
    PrintHelp(argv[0]);
    return 0;
  }
  struct DisplayAndBos display =
      InitDisplayAndBos(!p.external, p.vrr, p.refresh_rate);
  struct Gpu gpu = InitGpu(display.bos);

  ModesetOrDie(display);

  struct ScrollingState scrolling = InitScrollingState();
  scrolling.fractal_iteration = p.fractal_iteration;

  int frame = 0;
  int out_fence_fd = -1;
  struct timeval last_present_time = {0};

  while (frame++ < 60000) {
    int in_fence_fd =
        DrawFrame(&gpu, frame, out_fence_fd, last_present_time, &scrolling);
    out_fence_fd = -1;

    drmModeAtomicSetCursor(display.atomic_req, display.cursor);
    drmModeAtomicAddProperty(display.atomic_req, display.plane_id,
                             display.fb_property_id, display.fbs[frame % 2]);
    drmModeAtomicAddProperty(display.atomic_req, display.plane_id,
                             display.in_fence_property_id, in_fence_fd);
    drmModeAtomicAddProperty(display.atomic_req, display.crtc_id,
                             display.out_fence_property_id,
                             (uint64_t)&out_fence_fd);
    drmModeAtomicCommit(display.device_fd, display.atomic_req,
                        DRM_MODE_PAGE_FLIP_EVENT | DRM_MODE_ATOMIC_NONBLOCK,
                        &last_present_time);

    close(in_fence_fd);
    in_fence_fd = -1;

    struct pollfd pfd = {display.device_fd, POLLIN, 0};
    int ret = 0;
    do {
      ret = poll(&pfd, 1, -1);
    } while (ret == -1 && errno == EINTR);

    assert(ret > 0 && pfd.revents);
    drmEventContext ctx = {DRM_EVENT_CONTEXT_VERSION, VBlankHandler, 0,
                           PageFlipHandler, 0};
    drmHandleEvent(display.device_fd, &ctx);
  }
  DestroyScrollingState(scrolling);
  DestroyGpuObjects(gpu);
  DestroyDisplayAndBos(display);
  return 0;
}
